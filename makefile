all: process processA processB processC processD

main_process.o: main_process.c main_process_A.c main_process_B.c main_process_C.c main_process_D.c
	gcc -c main_process.c 
	gcc -Ofast -c main_process_A.c
	gcc -Ofast -c main_process_B.c
	gcc -fopenmp -Ofast -c main_process_C.c
	gcc -fopenmp -Ofast -c main_process_D.c
png_util.o: png_util.c
	gcc -l lpng16 -c png_util.c

process: main_process.o main_process_A.o main_process_B.o main_process_C.o main_process_D.o png_util.o
	gcc -o process -lm -l png16 main_process.o png_util.o
	gcc -o processA -lm -l png16 main_process_A.o png_util.o
	gcc -o processB -lm -l png16 main_process_B.o png_util.o
	gcc -fopenmp -o processC -lm -l png16 main_process_C.o png_util.o
	gcc -fopenmp -o processD -lm -l png16 main_process_D.o png_util.o
test: process 
	./process ./images/cube.png test0.png
	./processA ./images/cube.png test1.png
	./processB ./images/cube.png testB.png
	./processC ./images/cube.png testC.png
	./processD ./images/cube.png testD.png
clean:
	rm *.o
	rm process
	rm processA
	rm processB
	rm processC
	rm processD 
