#!/bin/bash
#benchmark script
echo "benchmark"
time ./benchmark.sh
echo "Compiler optimizer"
time ./benchmarkA.sh
echo "Loop switch"
time ./benchmarkB.sh
echo "OMP"
time ./benchmarkC.sh
echo "Median Filter code"
time ./benchmarkD.sh
